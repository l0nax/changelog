#!/bin/bash -
#===============================================================================
#
#          FILE: auto_testing.sh
#
#         USAGE: ./auto_testing.sh
#
#   DESCRIPTION: This Script auto generates some Changelog Entries.
#                Only use it for (simply) test the Changelog-Script.
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#  ORGANIZATION:
#       CREATED: 14.05.2019 22:35:43
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

Options="0
1
2
3
4
5
6
add
fix
change
deprecate
remove
security"

options=(${Options})
num_options=${#options[*]}
file_name=$(mktemp)

for i in $(seq 1 100); do
  echo -e "\e[0K\r ${i} %"
  text=$(date +%s | sha256sum | base64 | head -c 16)
  text="${text} $(date +%s | sha256sum | base64 | head -c 16)"

  _option="${options[$((RANDOM%num_options))]}"

  echo "${_option}" > ${file_name}
  ./changelog new "${_option}: ${text}" < ${file_name} > /dev/null

  sleep 0.1
done
